/**
 * Demonstrating process creation in Java.
 *
 * Figure 3.13
 *
 * @author Gagne, Galvin, Silberschatz
 * Operating System Concepts with Java - Eighth Edition
 * Copyright John Wiley & Sons - 2010.
 */

import java.io.*;
import java.util.Scanner;

public class OSProcess
{
	public static void main(String[] args) throws IOException {

		String directory = System.getProperty("user.dir");

		System.out.println("Working directory is: " + directory);

		ProcessBuilder pbProducer = new ProcessBuilder("java.exe", "-cp", directory, "Producer");

		Process processProducer = pbProducer.start();	// outputs continuesly "Hello"
		Scanner scanProducer =
			new Scanner(
		  	new BufferedInputStream(
					processProducer.getInputStream()));

	  ProcessBuilder pbConvertorCaps = new ProcessBuilder("java.exe", "-cp", directory, "Caps");

		Process processConvertorCaps = pbConvertorCaps.start();	 // inputs from stdin, capitalization
		PrintWriter printConvertorCaps =
						new PrintWriter(
								new BufferedWriter(
										new OutputStreamWriter(
												processConvertorCaps.getOutputStream())));
		Scanner scanConvertorCaps =
						new Scanner( new BufferedInputStream(
								processConvertorCaps.getInputStream()));

		ProcessBuilder pbConvertorReverse = new ProcessBuilder("java.exe", "-cp", directory, "Reverse");
		Process processConvertorReverse = pbConvertorReverse.start(); // inputs from stdin, reverses order
		PrintWriter printConvertorReverse =
		        new PrintWriter(
		            new BufferedWriter(
		                new OutputStreamWriter(
		                    processConvertorReverse.getOutputStream())));
		Scanner scanConvertorReverse =
		        new Scanner(
		            new BufferedInputStream(
		                processConvertorReverse.getInputStream()));

		String lineFromProducer = "";
	  String lineFromConvertorCaps = "";

		for(int i = 0; i < 20; i ++){
			if ( scanProducer.hasNextLine() ) {
      	lineFromProducer = scanProducer.nextLine();
      }
			printConvertorCaps.println(lineFromProducer);
			printConvertorCaps.flush();

			if ( scanConvertorCaps.hasNextLine() ) {
      	lineFromConvertorCaps = scanConvertorCaps.nextLine();
      }
			printConvertorReverse.println(lineFromConvertorCaps);
      printConvertorReverse.flush();

			if ( scanConvertorReverse.hasNextLine() ) {
        System.out.println(scanConvertorReverse.nextLine());
      }

			try
				{
					Thread.sleep(1500);
				}
				catch (InterruptedException e)
				{
					e.printStackTrace();
				}
		}

		processConvertorReverse.destroy();
    processConvertorCaps.destroy();
    processProducer.destroy();

	}
}
