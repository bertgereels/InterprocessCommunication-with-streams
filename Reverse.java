import java.io.*;
import java.util.Scanner;

public class Reverse {
  public static void main(String[] args) {
    BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(System.out));
		Scanner scan = new Scanner(System.in);

    while(true)
		{
			try {
				String test;
				test = scan.nextLine();

        StringBuilder input = new StringBuilder();
        input.append(test);

         input = input.reverse();

				System.out.println(input);
			}catch(Exception e) {
				System.out.println(e.getMessage());
			}

			//Sleep for a while
			try
			{
				Thread.sleep(100);
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}

		}
  }
}
