Het project bestaat uit 4 bestanden, namelijk: 
- Producer.java		-> Produceert constant "Hello"
- Caps.java		-> Zal de geproduceerde string binnenkrijgen en omzetten naar all caps "HELLO"
- Reverse.java		-> Krijgt "HELLO" binnen en zal dit omzetten naar "OLLEH"
- OSProcess.java 	-> Hierin worden de processen opgestart en worden de verschillende processen aan
			   elkaar gekoppeld met scanners en printers.

1)Om het geheel werkende te krijgen moet men eerst ervoor zorgen dat het commando 'javac' gekend is in
  de commandline. Dit kan men doen door: set "path=%path%;C:\Program Files\Java\jdk1.8.0_162\bin". 

2)Alle .java files moeten gecompileerd worden d.m.v. javac.
  Bv.: 'javac OSProcess.java'
  Alle .class files zijn ook alreeds bijgevoegd.

3)Als laatste moet men OSProcess uitvoeren. Dit kan door 'java OSProcess' te typen.
  Het is dus niet mogelijk om als argument een ander woord mee te geven, de producer zal altijd
  de string hello produceren.

4)In de commandline verschijnt nu "OLLEH".  
  Dit wordt 20 maal afgedrukt, daarna stopt het programma.


! Normaal moet men in OSProcess het path niet aanpassen op voorwaarde dat de .class bestanden allemaal in dezelfde map staan.

